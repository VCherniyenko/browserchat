package com.browserchat.browserchat.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.net.URLDecoder;

public final class URLUtils {
    private URLUtils () {}
    private static final Logger logger = LoggerFactory.getLogger(URLUtils.class);
    private static final String DECODE_FORMAT = "UTF-8";
    public static String getHostFromUrl(String url){
        try {
            URL pageUrl = new URL(URLDecoder.decode(url, DECODE_FORMAT));
            return pageUrl.getHost();
        } catch (Exception e) {
            logger.error("Can't get host from ulr " + url + "; " + e.getMessage(), e);
        }
        return url;
    }
}
