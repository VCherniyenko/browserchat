package com.browserchat.browserchat.config;

import com.browserchat.browserchat.model.ActiveUserPageViewInfo;
import com.browserchat.browserchat.service.ActiveUserPageViewInfoService;
import com.browserchat.browserchat.service.ViewPageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

@Component
public class SocketSessionListener {

    private static final Logger logger = LoggerFactory.getLogger(SocketSessionListener.class);
    @Autowired
    private SimpMessageSendingOperations messageSendingOperations;
    @Autowired
    private ActiveUserPageViewInfoService activeUserPageViewInfoService;
    @Autowired
    private ViewPageService viewPageService;

    @EventListener
    public void connect(SessionConnectedEvent event) {
        logger.info("Received a new web socket connection");
    }

    @EventListener
    public void disconnect(SessionDisconnectEvent event) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(event.getMessage());
        String username = (String) headerAccessor.getSessionAttributes().get("username");
        String userId = (String) headerAccessor.getSessionAttributes().get("userId");
        if (username != null) {
            ActiveUserPageViewInfo activeUser = activeUserPageViewInfoService.findUserPageViewInfoBySessionId(userId);
            viewPageService.updateCountAfterUserDisconnected(activeUser.getVisitingPages());
            activeUserPageViewInfoService.delete(activeUser);
            logger.info("User Disconnected : " + username);
            messageSendingOperations.convertAndSend("/topic/public", "");
        }
    }
}
