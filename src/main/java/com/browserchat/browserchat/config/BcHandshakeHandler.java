package com.browserchat.browserchat.config;

import com.browserchat.browserchat.model.GeoLocation;
import com.browserchat.browserchat.model.StompPrincipal;
import com.browserchat.browserchat.service.GeoLocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Map;
import java.util.UUID;

public class BcHandshakeHandler extends DefaultHandshakeHandler {

    private static final Logger logger = LoggerFactory.getLogger(BcHandshakeHandler.class);
    private final GeoLocationService geoLocationService;

    public BcHandshakeHandler(GeoLocationService geoLocationService) {
        this.geoLocationService = geoLocationService;
    }

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
        String sessionId = UUID.randomUUID().toString();
        String ip = (String) attributes.get("ip");
        logger.info("New user with id " + sessionId + ", ip " + ip);
        GeoLocation location = geoLocationService.getByIp(ip);
        location.setConnectionCount(location.getConnectionCount() + 1);
        if (location != null) {
            geoLocationService.save(location);
        }
        return new StompPrincipal(sessionId, ip);
    }
}
