package com.browserchat.browserchat.config;

import com.browserchat.browserchat.service.GeoLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
    @Autowired
    private GeoLocationService geoLocationService;
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/browserChat").addInterceptors(new IpHandshakeInterceptor()).setAllowedOrigins("*")
                .setHandshakeHandler(new BcHandshakeHandler(geoLocationService)).withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.enableStompBrokerRelay("/topic/", "/queue/").setUserDestinationBroadcast("/topic/unresolved-user-dest");
        registry.setApplicationDestinationPrefixes("/app/");
    }
}
