package com.browserchat.browserchat.repository;

import com.browserchat.browserchat.model.GeoLocation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeoLocationRepository extends CrudRepository<GeoLocation, String> {

}
