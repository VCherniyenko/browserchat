package com.browserchat.browserchat.repository;

import com.browserchat.browserchat.model.ViewPageInfo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ViewPageInfoRepository extends CrudRepository<ViewPageInfo, String> {

    ViewPageInfo findViewPageInfoByHostAndPageUrl(String host, String pageUrl);
}
