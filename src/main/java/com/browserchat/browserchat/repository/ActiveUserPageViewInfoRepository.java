package com.browserchat.browserchat.repository;

import com.browserchat.browserchat.model.ActiveUserPageViewInfo;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActiveUserPageViewInfoRepository extends CrudRepository<ActiveUserPageViewInfo, String> {

    @Query("{ '_id' : ?0 },{ $push : { visitingPages: { $each: [?1] }}}, {'upsert' : true}")
    void updateUserPageViewInfo(String sessionId, List<String> pageUrls);

    @Query(delete = true, value = "{ '_id' : ?0 }")
    void deleteById(String sessionId);
}
