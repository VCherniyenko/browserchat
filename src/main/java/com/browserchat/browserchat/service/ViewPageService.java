package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.ViewPageInfo;

import java.util.Collection;

public interface ViewPageService {

    ViewPageInfo pageOpened(String pageUrl, String sessionId);

    ViewPageInfo pageClosed(String pageUlr);

    void updateCountAfterUserDisconnected(Collection<String> urls);
}
