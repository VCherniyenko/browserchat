package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.ViewPageInfo;
import com.browserchat.browserchat.repository.ViewPageInfoRepository;
import com.browserchat.browserchat.utils.URLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class ViewPageServiceImpl implements ViewPageService {
    private static final Logger logger = LoggerFactory.getLogger(ViewPageServiceImpl.class);
    @Autowired
    private ActiveUserPageViewInfoService activeUserPageViewInfoService;
    @Autowired
    private ViewPageInfoRepository viewPageInfoRepository;
    @Autowired
    private MongoTemplate mongoTemplate;


    @Override
    public ViewPageInfo pageOpened(String pageUrl, String sessionId) {
        activeUserPageViewInfoService.updateUserPageViewInfo(sessionId, Collections.singletonList(pageUrl));
        String host = URLUtils.getHostFromUrl(pageUrl);
        ViewPageInfo viewPageInfo = viewPageInfoRepository.findViewPageInfoByHostAndPageUrl(host, pageUrl);
        if (viewPageInfo == null) {
            viewPageInfo = new ViewPageInfo(pageUrl);
        } else {
            viewPageInfo.setOnlineCount(viewPageInfo.getOnlineCount() + 1);
            viewPageInfo.setTotalCount(viewPageInfo.getTotalCount() + 1);
        }
        viewPageInfoRepository.save(viewPageInfo);
        return viewPageInfo;
    }

    @Override
    public ViewPageInfo pageClosed(String pageUrl) {
        String host = URLUtils.getHostFromUrl(pageUrl);
        ViewPageInfo viewPageInfo = viewPageInfoRepository.findViewPageInfoByHostAndPageUrl(host, pageUrl);
        if (viewPageInfo == null) {
            logger.error("Not registered page is closed: " + pageUrl);
        } else {
            viewPageInfo.setOnlineCount(viewPageInfo.getOnlineCount() - 1);
            viewPageInfoRepository.save(viewPageInfo);
        }
        return viewPageInfo;
    }

    @Override
    public void updateCountAfterUserDisconnected(Collection<String> urls) {
        Query query = new Query(Criteria.where("pageUrl").in(urls));
        Update update = new Update().inc("onlineCount", -1);
        mongoTemplate.updateMulti(query, update, ViewPageInfo.class);
    }
}
