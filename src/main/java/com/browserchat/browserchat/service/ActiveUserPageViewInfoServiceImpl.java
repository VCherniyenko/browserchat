package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.ActiveUserPageViewInfo;
import com.browserchat.browserchat.repository.ActiveUserPageViewInfoRepository;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ActiveUserPageViewInfoServiceImpl implements ActiveUserPageViewInfoService {
    @Autowired
    private ActiveUserPageViewInfoRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * {@inheritDoc}
     *
     * @param userPageViewInfo - document to save;
     */
    @Override
    public void save(ActiveUserPageViewInfo userPageViewInfo) {
        repository.save(userPageViewInfo);
    }

    /**
     * {@inheritDoc}
     * @param sessionId - document ID;
     * @return
     */
    @Override
    public ActiveUserPageViewInfo findUserPageViewInfoBySessionId(String sessionId) {
        return repository.findById(sessionId).get();
    }

    /**
     * {@inheritDoc}
     * @param sessionId - ID of the document;
     * @param pageUrl
     */
    @Override
    public void updateUserPageViewInfo(String sessionId, List<String> pageUrl) {

        Update update = new Update();
        Document each = new Document("$each", pageUrl.toArray());
        update.addToSet("visitingPages", each);
        Query query = new Query(Criteria.where("_id").is(sessionId));
        mongoTemplate.updateFirst(query, update, ActiveUserPageViewInfo.class);
    }

    /**
     * {@inheritDoc}
     * @param sessionId - ID of the document;
     */
    @Override
    public void deleteBySessionId(String sessionId) {
        repository.deleteById(sessionId);
    }

    /**
     * {@inheritDoc}
     * @param activeUserPageViewInfo - document to delete;
     */
    @Override
    public void delete(ActiveUserPageViewInfo activeUserPageViewInfo) {
        repository.delete(activeUserPageViewInfo);
    }
}
