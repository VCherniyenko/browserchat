package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.User;

public interface UserService {
    void save(User user);
}
