package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.GeoLocation;
import com.browserchat.browserchat.repository.GeoLocationRepository;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

@Service
public class GeoLocationServiceImpl implements GeoLocationService {

    private static final Logger logger = LoggerFactory.getLogger(GeoLocationServiceImpl.class);
    private static final String IP_API_URL = "http://api.ipstack.com/%s?access_key=%s";
    private static final HttpClient HTTP_CLIENT = HttpClientBuilder.create().build();
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Value("${geolocation.key}")
    private String GEO_API_KEY;

    @Autowired
    private GeoLocationRepository geoLocationRepository;

    /**
     * {@inheritDoc}
     *
     * @param ip - address to get info about;
     * @return
     */
    @Override
    public GeoLocation getByIp(String ip) {
        try {
            GeoLocation geoLocation;
            Optional<GeoLocation> locationFromDb = geoLocationRepository.findById(ip);
            if (locationFromDb.isPresent()) {
                geoLocation = locationFromDb.get();
                geoLocation.setLastConnectionDate(new Date());
            } else {
                geoLocation = getGeoLocationFromExternalService(ip);
            }
            return geoLocation;
        } catch (Exception ex) {
            logger.error("Can't get location for ip " + ip + ". " + ex.getMessage(), ex);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * @param geoLocation - object to save;
     */
    @Override
    public void save(GeoLocation geoLocation) {
        geoLocationRepository.save(geoLocation);
    }

    /**
     * Method fetches details using "ipstack" external service;
     * @param ip - address to get info about;
     * @return - GeoLocation instance with details about ip;
     * @throws IOException
     */
    private GeoLocation getGeoLocationFromExternalService(String ip) throws IOException {
        GeoLocation geoLocation;
        String requestUrl = String.format(IP_API_URL, ip, GEO_API_KEY);
        HttpGet httpGet = new HttpGet(requestUrl);
        HttpResponse httpResponse = HTTP_CLIENT.execute(httpGet, new BasicHttpContext());
        String responseString;
        if (httpResponse.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
            throw new RuntimeException("Sorry! Response Error. Status Code: " +
                    httpResponse.getStatusLine().getStatusCode());
        }
        responseString = EntityUtils.toString(httpResponse.getEntity());
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        geoLocation = MAPPER.readValue(responseString, GeoLocation.class);
        return geoLocation;
    }
}
