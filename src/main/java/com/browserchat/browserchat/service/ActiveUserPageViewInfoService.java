package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.ActiveUserPageViewInfo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ActiveUserPageViewInfoService {
    /**
     * Save ActiveUserPageViewInfo to db;
     *
     * @param userPageViewInfo - document to save;
     */
    void save(ActiveUserPageViewInfo userPageViewInfo);

    /**
     * Method fetch ActiveUserPageViewInfo document by ID;
     * @param sessionId - document ID;
     * @return - document from db;
     */
    ActiveUserPageViewInfo findUserPageViewInfoBySessionId(String sessionId);

    /**
     * Method updates visitingPages in a ActiveUserPageViewInfo document;
     * @param sessionId - ID of the document;
     * @param pageUrls - list of visited pages to be saved in document with <code>sessionId</code>}
     */
    void updateUserPageViewInfo(String sessionId, List<String> pageUrls);

    /**
     * Delete ActiveUserPageViewInfo document by session id which is ID in this document;
     * @param sessionId - id of the document;
     */
    void deleteBySessionId(String sessionId);

    /**
     * Method deletes passed ActiveUserPageViewInfo document from db;
     * @param activeUserPageViewInfo - document to delete;
     */
    void delete(ActiveUserPageViewInfo activeUserPageViewInfo);
}
