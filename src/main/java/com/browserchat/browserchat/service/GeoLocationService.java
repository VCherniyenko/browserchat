package com.browserchat.browserchat.service;

import com.browserchat.browserchat.model.GeoLocation;

public interface GeoLocationService {
    /**
     * Method gets geolocation details by ip-address, which is ID in GeoLocation document;
     * If document from db is null then we use external service to get all details about current ip;
     * In case document found, update connectionCount field;
     *
     * @param ip - address to get info about;
     * @return - GeoLocation instance with ip details. Also possible to get null, if exception occurred during process;
     */
    GeoLocation getByIp(String ip);

    /**
     * Save geolocation to db;
     * @param geoLocation - object to save;
     */
    void save(GeoLocation geoLocation);
}
