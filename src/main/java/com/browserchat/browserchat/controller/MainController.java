package com.browserchat.browserchat.controller;


import com.browserchat.browserchat.model.ActiveUserPageViewInfo;
import com.browserchat.browserchat.model.ChatMessage;
import com.browserchat.browserchat.model.StompPrincipal;
import com.browserchat.browserchat.model.ViewPageInfo;
import com.browserchat.browserchat.service.ActiveUserPageViewInfoService;
import com.browserchat.browserchat.service.UserServiceImpl;
import com.browserchat.browserchat.service.ViewPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

import static java.util.Collections.emptySet;

@Controller
public class MainController {

    @Autowired
    private SimpMessageSendingOperations messagingTemplate;

    @Autowired
    private ViewPageService viewPageService;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private ActiveUserPageViewInfoService activeUserPageViewInfoService;

    @Autowired
    private SimpUserRegistry simpUserRegistry;

    @GetMapping("/connectedUsersCount")
    @ResponseBody
    public Integer getConnectedUsersCount() {
        return simpUserRegistry.getUserCount();
    }

    @MessageMapping("/chat/{destination}")
    @SendTo("/topic/greetings")
    public void sendMsg(ChatMessage msg, StompPrincipal principal, @DestinationVariable("destination") String destination) {
        msg.setFromSessionId(principal.getName());
        msg.setFromName(principal.getDisplayName());
        msg.setSenderIp(principal.getUserIp());
        Message<ChatMessage> operation = MessageBuilder.withPayload(msg).build();
        messagingTemplate.send("/topic/" + destination, operation);
    }

    @MessageMapping("/private")
    public void sendPrivateMsg(ChatMessage msg, StompPrincipal principal) {
        msg.setFromSessionId(principal.getName());
        msg.setFromName(principal.getDisplayName());
        msg.setSenderIp(principal.getUserIp());
        messagingTemplate.convertAndSendToUser(msg.getToSessionId(), "/queue/private", msg);
    }

    @MessageMapping("/openedPage")
    @SendTo("/topic/updateOpened")
    public ViewPageInfo openedPage(String pageUrl, Principal principal) {
        return viewPageService.pageOpened(pageUrl, principal.getName());
    }

    @MessageMapping("/closedPage")
    @SendTo("/topic/updateOpened")
    public ViewPageInfo closedPage(String pageUrl) {
        return viewPageService.pageClosed(pageUrl);
    }

    @MessageMapping("/newUser")
    public Object addUser(String userName, StompPrincipal principal, SimpMessageHeaderAccessor headerAccessor) {
        // Add username in web socket session
        headerAccessor.getSessionAttributes().put("username", userName);
        String userId = principal.getName();
        headerAccessor.getSessionAttributes().put("userId", userId);
        principal.setDisplayName(userName);
        activeUserPageViewInfoService.save(new ActiveUserPageViewInfo(userId, userName, emptySet()));
        return principal;
    }
//    @MessageMapping("/chats/{chatRoomId}")
//    public void handleChat(@Payload ChatMessage message, @DestinationVariable("chatRoomId") String chatRoomId, MessageHeaders messageHeaders, Principal user) {
//        logger.info(messageHeaders.toString());
//        this.simpMessagingTemplate.convertAndSend("/topic/chats." + chatRoomId, "[" + getTimestamp() + "]:" + user.getName() + ":" + message.getMessage());
//    }

}
