package com.browserchat.browserchat.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document(collection = "ActiveUserPageViewInfo")
public class ActiveUserPageViewInfo {
    public ActiveUserPageViewInfo() {
    }

    public ActiveUserPageViewInfo(String id, String username, Set<String> visitingPages) {
        this.id = id;
        this.username = username;
        this.visitingPages = visitingPages;
    }

    @Id
    private String id;
    @Indexed
    private String username;
    private Set<String> visitingPages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<String> getVisitingPages() {
        return visitingPages;
    }

    public void setVisitingPages(Set<String> visitingPages) {
        this.visitingPages = visitingPages;
    }
}
