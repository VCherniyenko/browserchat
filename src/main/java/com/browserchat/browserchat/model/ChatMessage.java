package com.browserchat.browserchat.model;

public class ChatMessage {
    private String fromSessionId;
    private String fromName;
    private String toName;
    private String toSessionId;
    private String text;
    private boolean isPrivate;
    private String senderIp;

    public String getFromSessionId() {
        return fromSessionId;
    }

    public void setFromSessionId(String fromSessionId) {
        this.fromSessionId = fromSessionId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

    public String getToSessionId() {
        return toSessionId;
    }

    public void setToSessionId(String toSessionId) {
        this.toSessionId = toSessionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getSenderIp() {
        return senderIp;
    }

    public void setSenderIp(String senderIp) {
        this.senderIp = senderIp;
    }
}
