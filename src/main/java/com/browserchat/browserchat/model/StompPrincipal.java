package com.browserchat.browserchat.model;

import java.security.Principal;

public class StompPrincipal implements Principal {

    public StompPrincipal(String name) {
        this.name = name;
    }

    public StompPrincipal(String name, String ip) {
        this.name = name;
        this.userIp = ip;
    }

    private String name;

    private String displayName;

    private String userIp;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getUserIp() {
        return userIp;
    }

    public void setUserIp(String userIp) {
        this.userIp = userIp;
    }
}
