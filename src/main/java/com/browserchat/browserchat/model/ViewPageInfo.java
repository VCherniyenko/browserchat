package com.browserchat.browserchat.model;

import com.browserchat.browserchat.utils.URLUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.net.URI;

@Document(collection = "pageViewInfo")
public class ViewPageInfo {

    @Id
    private String id;
    private String pageUrl;
    @Indexed
    private String host;
    //Number of users viewing this page;
    private int onlineCount = 1;
    private int totalCount = 1;

    public ViewPageInfo() {
    }

    public ViewPageInfo(String pageUrl, int onlineCount) {
        this.pageUrl = pageUrl;
        this.host = URLUtils.getHostFromUrl(pageUrl);
        this.onlineCount = onlineCount;
    }

    public ViewPageInfo(String pageUrl) {
        this.pageUrl = pageUrl;
        this.host = URLUtils.getHostFromUrl(pageUrl);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public int getOnlineCount() {
        return onlineCount;
    }

    public void setOnlineCount(int onlineCount) {
        this.onlineCount = onlineCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
