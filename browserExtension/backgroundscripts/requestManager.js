const requestManager = {
    post: (url, data, callback) => {
        fetch(`${url}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response) => {
            if (!response.ok) throw new Error(response.statusText);
            return response.json();
        }).then(data=> {
            if (callback && typeof callback === 'function') callback(data);
        }).catch(error=> {
            console.log(error)
        })
    },
    get: (url, callback) => {
        fetch(`${url}`).then(response => {
            if (!response.ok) throw new Error(response.statusText);
            return response.json();
        }).then(data=> {
            if (callback && typeof callback === 'function') callback(data);
        })
    }

};

let getRandomUserName = () => {
    requestManager.post('https://api.codetunnel.net/random-nick', {"sizeLimit": 15})
};