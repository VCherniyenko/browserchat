console.log('start extension');
let ws = new SockJS('http://localhost:8080/browserChat');
let stompClient = Stomp.over(ws);
let subscriptionList = [];
function getUserId() {
    let userId = new Date().getTime();
    let localId = localStorage.getItem('userId');
    if (!localId) {
        localStorage.setItem('userId', userId)
    } else {
        userId = localId;
    }
    return userId;
}

let userId = getUserId();
stompClient.connect({}, function (frame) {
    stompClient.subscribe("/topic/browserChat", message => {
        console.log(message);
    }, {});
    stompClient.subscribe('/topic/updateOpened', (message) => {
        console.log(message)
    }, {});
    subscribeTo(stompClient, '/user/queue/private', (message) => {
        console.log(message)
    }, {})
}, function (error) {
    console.log(`Error stomp connecting :${error.body} `)
});

let subscribeTo = (client, chanel, msgHandler, opts) => {
    client.subscribe(chanel, (msg)=> {
        console.log(msg);
        msgHandler(msg)
    }, opts);
};

let sendChatMsg = (text) => {
    stompClient.send('/topic/browserChat', {}, text)
};
let sendStompMsg = (uri, text) => {
    stompClient.send(`/topic/${uri}`, {}, text)
};

let sendAppMsg = (uri, text) => {
    stompClient.send(`/app/${uri}`, {}, text)
};

let sendPrivateMsg = (user, msg) => {
    stompClient.send(`/user/${user}/queue/private`, {}, msg);
};
chrome.tabs.onCreated.addListener(tab => {
});

let sendMsgToContent = (pageUrl, content) => {
    let msg = JSON.parse(JSON.stringify(content));
    findTabByUrl(pageUrl, (tabs) => {
        tabs.forEach(tab => {
            chrome.tabs.sendMessage(tab.id, {msg: 'userSay', content: msg}, {})
        })
    });
};


let findTabByUrl = (url, tabsHandler) => {
    // We are not using chrome.tabs.query because it doesn't match urls with
    // # symbols (https://www.olx.ua/hobbi-otdyh-i-sport/sport-otdyh/futbol/krivoyrog/#from404)
    chrome.tabs.query({}, function (tabs) {
        if (tabs && tabs.length) tabsHandler(tabs.filter(function (tab) {
            return tab.url === url
        }));
        else console.warn(`Tabs with url ${url} not found`);
    })
};

let setUserName = userName => {
    sendAppMsg('newUser', userName);
};

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    if(changeInfo.status === 'complete') {
        let encodedUrl = encodeURIComponent(tab.url);
        if (!subscriptionList.includes(encodedUrl)) {
            subscriptionList.push(encodedUrl);
            sendAppMsg('openedPage', encodedUrl);
            subscribeTo(stompClient, `/topic/${encodedUrl}`, (msg) => {
                sendMsgToContent(tab.url, msg);
            }, {});
        }
    }
});