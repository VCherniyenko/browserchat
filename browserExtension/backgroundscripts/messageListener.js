function handleMessage(request, sender, sendResponse) {
    let encodedUrl = encodeURIComponent(request.url);
    switch (request.msg) {
        case 'openedPage':
        {
            console.log('opened url: ' + request.url);
            if (!subscriptionList.includes(encodedUrl)) {
                subscriptionList.push(encodedUrl);
                sendAppMsg('openedPage', encodedUrl);
                subscribeTo(stompClient, `/topic/${encodedUrl}`, (msg) => {
                    sendMsgToContent(request.url, msg);
                }, {});
            }
            break;
        }
        case 'sendPublic':
        {
            sendStompMsg(encodedUrl, request.content);
            break;
        }
        case 'sendPrivate':
        {
            break;
        }
    }
    sendResponse({});
}
chrome.runtime.onMessage.addListener(handleMessage);
